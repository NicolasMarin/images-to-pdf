# Images To Pdf

Un programa Java para convertir los archivos de imágenes en un directorio en un archivo PDF.

*Leer esto en otro idioma: [Ingles](README.md)

## Instrucciones de uso

La clase "Conversor" tiene el método "createPDFs", que recibe un array de parámetros "arg", que especifican cómo se crearán los archivos pdf.
Los valores definidos por el array "arg" son:
1. root: ruta al directorio con los archivos imagen que se utilizarán para crear un archivo pdf.
2. outputFileName: nombre sin extensión del archivo pdf a crear.
3. outputFileNameMode: Según el valor recibido, el nombre del archivo pdf se puede formar de 2 maneras:
	* Conversor.COMPLEX_NAME: utilizando el valor en "outputFileName", más el nombre del directorio de imágenes. Por ejemplo, en este modo, si el nombre del directorio de imágenes es "01" y el valor en "outputFileName" es "myNewPdf", el nombre utilizado en el nuevo archivo es "myNewPdf 01".
	* usando cualquier otro valor, el nombre será solo el valor en "outputFileName".
4. outputFilePath: ruta donde se creará el archivo pdf. Si el valor está vacío, usa la ruta del directorio de imágenes.
5. subdirectoriesMode: Según el valor recibido, puede ser uno de los dos modos:
	* Conversor.SUBDIRECTORIES_ON: en este modo, el método, para cada subdirectorio del directorio pasado en "root", usa sus imágenes y crea un archivo pdf.
	* Conversor.SUBDIRECTORIES_OFF: en este modo, el método solo crea un único archivo pdf con las imágenes en el directorio pasado en "root".
