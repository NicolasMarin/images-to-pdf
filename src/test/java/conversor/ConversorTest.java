package conversor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.Test;

import com.itextpdf.text.DocumentException;

public class ConversorTest {
	
	//TestClass
	@Test
	public void testClass() {
		Conversor conversor = new Conversor();
		assertTrue(conversor != null);
	}
	
	
	//CreatePDFs
	@Test
	public void testCreatePDFsArrayNull() throws MalformedURLException, DocumentException, IOException {
		assertTrue("There is no parameters.".equals(Conversor.createPDFs(null)));
	}
	
	@Test
	public void testCreatePDFsArrayEmpty() throws MalformedURLException, DocumentException, IOException {
		assertTrue("There is no parameters.".equals(Conversor.createPDFs(new String[]{})));
	}
	
	@Test
	public void testCreatePDFsInvalidPath() throws MalformedURLException, DocumentException, IOException {
		assertTrue("The Path: 'myInvalidPath' is not Valid.".equals(Conversor.createPDFs(new String[]{"myInvalidPath"})));
	}
	
	@Test
	public void testCreatePDFsRootIsFile() throws MalformedURLException, DocumentException, IOException {
		assertTrue(("The Path: 'pom.xml' is not from a Directory.")
				.equals(Conversor.createPDFs(new String[]{"pom.xml"})));
	}
	
	@Test
	public void testCreatePDFsRootIsEmpty() throws MalformedURLException, DocumentException, IOException {
		assertTrue(("The Directory: 'src' is empty.")
				.equals(Conversor.createPDFs(new String[]{"src", "pdfName", "", "", Conversor.SUBDIRECTORIES_OFF})));
	}
		
	
	//IsSubDirModeOn
	@Test
	public void testIsSubDirModeOnTrue() {
		String[] arg = {"imageDirectory", "MypdfName", "", "C:\\Users", Conversor.SUBDIRECTORIES_ON};
		assertTrue(Conversor.isSubDirModeOn(arg));
	}
	
	@Test
	public void testIsSubDirModeOnFalse() {
		String[] arg = {"imageDirectory", "MypdfName", "", "C:\\Users", Conversor.SUBDIRECTORIES_OFF};
		assertFalse(Conversor.isSubDirModeOn(arg));
	}
	
	
	//GetSubdirectoriesMode
	@Test
	public void testGetSubdirectoriesModeArrayLengthLowerThanFive() {
		String[] arg = {"imageDirectory", "MypdfName", "", "C:\\Users"};
		assertTrue(Conversor.SUBDIRECTORIES_OFF.equals(Conversor.getSubdirectoriesMode(arg)));
	}
	
	@Test
	public void testGetSubdirectoriesModeArrayLengthBiggerThanFourModeOn() {
		String[] arg = {"imageDirectory", "MypdfName", "", "C:\\Users", Conversor.SUBDIRECTORIES_ON};
		assertTrue(Conversor.SUBDIRECTORIES_ON.equals(Conversor.getSubdirectoriesMode(arg)));
	}
	
	@Test
	public void testGetSubdirectoriesModeArrayLengthBiggerThanFourModeOff() {
		String[] arg = {"imageDirectory", "MypdfName", "", "C:\\Users", Conversor.SUBDIRECTORIES_OFF};
		assertTrue(Conversor.SUBDIRECTORIES_OFF.equals(Conversor.getSubdirectoriesMode(arg)));
	}
	
	
	//GetOutputFilePath
	@Test
	public void testGetOutputFilePathArrayLengthLowerThanFour() {
		String[] arg = {"imageDirectory", "MypdfName", ""};
		assertTrue(new File("imageDirectory").equals(Conversor.getOutputFilePath(arg)));
	}
	
	
	@Test
	public void testGetOutputFilePathFourthElementEmpty() {
		String[] arg = {"imageDirectory", "MypdfName", "", ""};
		assertTrue(new File("imageDirectory").equals(Conversor.getOutputFilePath(arg)));
	}
	
	@Test
	public void testGetOutputFilePathFourthElementNotEmpty() {
		String[] arg = {"imageDirectory", "MypdfName", "", "C:\\Users"};
		assertTrue(new File("C:\\Users").equals(Conversor.getOutputFilePath(arg)));
	}
	
	
	//GetNameToPdf
	
	@Test
	public void testGetNameToPdfOneElementArray() {
		assertTrue("myFolder.pdf".equals(
				Conversor.getNameToPdf(new String[]{"myFolder"})));
	}
	
	@Test
	public void testGetNameToPdfTwoElementArray() {
		assertTrue("myPdfName.pdf".equals(
				Conversor.getNameToPdf(new String[]{"myFolder", "myPdfName"})));
	}
	
	@Test
	public void testGetNameToPdfMoreThanTwoElementArrayComplexName() {
		assertTrue("myPdfName 01.pdf".equals(
				Conversor.getNameToPdf(new String[]{"01", "myPdfName ", Conversor.COMPLEX_NAME})));
	}
	
	@Test
	public void testGetNameToPdfMoreThanTwoElementArraySimpleName() {
		assertTrue("myPdfName .pdf".equals(
				Conversor.getNameToPdf(new String[]{"01", "myPdfName ", ""})));
	}
	
	@Test
	public void testGetNameToPdfEmptyArray() {
		assertTrue("".equals(
				Conversor.getNameToPdf(new String[]{})));
	}
	
	
	//IsAnValidExtension
	@Test
	public void testIsAnValidExtensionJpg() {
		assertTrue(Conversor.isAnValidExtension(new File("file.jpg")));
	}
	
	@Test
	public void testIsAnValidExtensionPng() {
		assertTrue(Conversor.isAnValidExtension(new File("file.png")));
	}
	
	@Test
	public void testIsAnValidExtensionNotValid() {
		assertFalse(Conversor.isAnValidExtension(new File("file.txt")));
	}

}
