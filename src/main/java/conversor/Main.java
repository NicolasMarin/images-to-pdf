package conversor;

public class Main {
	
	public static void main(String arg[]) throws Exception {
		System.out.println(Conversor.createPDFs(new String[]
		    	{"PathToImagesDirectory", 
		    			"NameToThePdfFile", 
		    			Conversor.COMPLEX_NAME, 
		    			"PathToThePdfFile",
		    			Conversor.SUBDIRECTORIES_OFF}));
	}

}
