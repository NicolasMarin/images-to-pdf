package conversor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public class Conversor {
	
	public static String COMPLEX_NAME = "COMPLEX_NAME";
	public static String SUBDIRECTORIES_ON = "SUBDIRECTORIES_ON";
	public static String SUBDIRECTORIES_OFF = "SUBDIRECTORIES_OFF";
	
	/**
	 * This is the class main method.
	 * This method verify that the parameters are valid, 
	 * in which case call the methods to create the requested pdf files.
	 * @param arg, array with arguments specifying how to create pdf files.
	 * @return a message informing about the result of the method execution 
	 */
	public static String createPDFs(String arg[]) throws MalformedURLException, DocumentException, IOException {
		if(arg == null || arg.length == 0) return "There is no parameters.";
		
		File root = new File(arg[0]);
		if(!root.exists()) return "The Path: '" + arg[0] + "' is not Valid.";
		if(!root.isDirectory()) return "The Path: '" + arg[0] + "' is not from a Directory.";
		
		List<String> files = new ArrayList<String>();
		List<String> subdirectories = new ArrayList<String>();
		for(File file: root.listFiles()) {
    		if(isAnValidExtension(file)) files.add(file.getName());
    		if(isSubDirModeOn(arg) && file.isDirectory()) subdirectories.add(file.getName());
        }
		
		if(files.isEmpty() && subdirectories.isEmpty()) return "The Directory: '" + arg[0] + "' is empty.";
		
		String answer = "";
		if(isSubDirModeOn(arg)) answer += handleSubDirectories(subdirectories, arg);
		if(!files.isEmpty()) answer += createOnePDF(arg, files, root);
		
		return answer;
	}
	
	/**
	 * This method handles to make the recursive calls to the "createPDFs" method 
	 * when is necessary to create pdf files in the subdirectories of root
	 * @param subdirectories, list with the names of the subdirectories of the current directory
	 * @param arg, array with arguments that are pass in the recursive call
	 * @return a message informing about the result of the method execution
	 */
	private static String handleSubDirectories(List<String> subdirectories, String[] arg) throws MalformedURLException, DocumentException, IOException {
		String answer = ""; 
		for(String currentDir: subdirectories) {
			String[] params = new String[]
			    	{arg[0] + File.separator + currentDir, arg[1], arg[2], arg[3], arg[4]};
			answer += createPDFs(params) + "\n";
		}
		return answer;
	}

	/**
	 * Create one pdf file using a set of images files (.jpg or .npg)
	 * @param arg, the array used to get the name and path of the pdf file to create
	 * @param files, the list with the names of the images files to use to create the pdf file
	 * @param root, the directory that contains the images to use to create the pdf file
	 * @return a message informing about the result of the method execution 
	 */
	private static String createOnePDF(String[] arg, List<String> files, File root) throws DocumentException, MalformedURLException, IOException {
		String outputFileName = getNameToPdf(arg);
    	File outputFilePath = getOutputFilePath(arg);
    	
    	if(outputFileName.isEmpty() || !outputFilePath.exists()) return "The name/path for the pdf file for the directory: '" + root.getAbsolutePath() + "' is not valid.";
    	
    	Document document = new Document();
    	FileOutputStream fos = new FileOutputStream(new File(outputFilePath, outputFileName));
    	PdfWriter.getInstance(document, fos);
        document.open();
        for (String f : files) {
        	Image image = Image.getInstance(new File(root, f).getAbsolutePath());
            image.setAbsolutePosition(0, 0);
            Rectangle rectangle = new Rectangle(image.getWidth(), image.getHeight());
            document.setPageSize(rectangle);
            document.newPage();
            document.add(image);
        }
        document.close();
        fos.close();
        return "File: '" + new File(outputFilePath, outputFileName).getAbsolutePath() + "' created.";
	}

	/**
	 * @param arg array to use to check if the subdirectories mode is on
	 * @return whether the subdirectories mode is on or not
	 */
	protected static boolean isSubDirModeOn(String[] arg) {
		return getSubdirectoriesMode(arg).equals(SUBDIRECTORIES_ON);
	}

	/**
	 * @param arg array with which to use its elements to know the mode of the subdirectories
	 * @return the mode of the subdirectories
	 */
	protected static String getSubdirectoriesMode(String[] arg) {
		return (arg.length > 4 && arg[4].equalsIgnoreCase(SUBDIRECTORIES_ON)) ? 
				SUBDIRECTORIES_ON : SUBDIRECTORIES_OFF;
	}

	/**
	 * @param arg array with which to use its elements to create the path of the pdf file
	 * @return the path to use for the pdf file
	 */
	protected static File getOutputFilePath(String[] arg) {
		return new File(arg[(arg.length < 4 || arg[3].isEmpty()) ? 0 : 3]);
	}

	/**
	 * @param arg array with which to use its elements to create the name of the pdf file
	 * @return the name (including extension) to use for the pdf file 
	 */
	protected static String getNameToPdf(String[] arg) {
		if(arg.length > 2) {
			String mode = arg[2];
			return arg[1] + (mode.equalsIgnoreCase(COMPLEX_NAME) ? 
					new File(arg[0]).getName() : "") + ".pdf";
		}
		switch (arg.length) {
			case 1:  return new File(arg[0]).getName() + ".pdf";
			case 2:  return arg[1] + ".pdf";
			default: return "";
		}
	}
	
	/**
	 * @param file A file to check if it's extension is valid.
	 * @return whether the file extension is valid or not, checking if the extension is .jpg or .png
	 */
	protected static boolean isAnValidExtension(File file) {
		return FilenameUtils.getExtension(file.getName()).equals("jpg") 
				|| FilenameUtils.getExtension(file.getName()).equals("png");
	}

}
