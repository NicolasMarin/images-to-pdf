# Images To Pdf

A Java Program to Convert the Images files in a Directory into a PDF file.

*Read this in other language: [Spanish](README.es.md)

## Instructions for use

The "Conversor" class has the "createPDFs" method, that receive an "arg" array of parameters, which specify how the pdf files will be created.
The values that are defined by the "arg" array are: 
1. root: path to the directory with the image files to use to create a pdf file.
2. outputFileName: name without extension of the pdf file to be created.
3. outputFileNameMode: According to the received value, the name of the pdf file can be formed in 2 ways:
	* Conversor.COMPLEX_NAME: using the value in "outputFileName", plus the name of the images directory. For example, in this mode, if the name of the image directory is "01" and the value in "outputFileName" is "myNewPdf ", the name used in the new file is "myNewPdf 01".
	* using any other value, the name will be just the value on "outputFileName".
4. outputFilePath: path where the pdf file will be created. If the value is empty, it use the image directory path.
5. subdirectoriesMode: According to the received value, it can be one of two modes:
	* Conversor.SUBDIRECTORIES_ON: in this mode, the method, for each subdirectory of the directory passed in "root", uses its images and creates a pdf file.
	* Conversor.SUBDIRECTORIES_OFF: in this mode, the method only create a single pdf file with the images in the directory passed in "root".

